# Le Kube
Implementation of an IoT solution for growing plants.\
<img src="docs/kube_face.jpg" alt="drawing" width="400"/>
<img src="docs/kube_dos.jpg" alt="drawing" width="400"/>\
The data collected by the sensors are sent via wifi and accessible online, as well as on the Oled screen.
Variables monitored by the captors:
* `soil moisture
* `light
* `temperature
A water pump is connected to an optocoupler to automate the watering.

## Block diagram
![](docs/block_diagram.png)

## Electric diagram
![](docs/elec_diagram.png)
